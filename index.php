<html>
<head>
<meta charset="UTF-8">

	<script src="js/jquery.min.js"></script>
	<script>
        //$(document).ready(function(){

		function parseJsonData(color, mode){
			
			var data = $('#pointsData').val();

			obj = JSON.parse(data);
			var points = obj.points;

			var table = "<table border=1 width=100%><tr><th>Skywards Saver Fare</th><th>Skywards Flex Fare</th></tr><tr><td>" + points[color + '_' + mode ]['Saver Skyware Miles'] + "</td><td>" + points[color + '_' + mode ]['Flex Skyware Miles'] +"</td></tr><tr><td>" + points[color + '_' + mode ]['Saver Tier Miles'] + "</td><td>" + points[color + '_' + mode ]['Flex Tier Miles'] +"</td></tr></table>";
				
			$('#pointsTable').html(table);
		}

            function getParsedData(){
		$('#pointsTable').html('<h2>Oopss...Network is a running a little slow... Please hold till i fetch the data</h2>');
                $.ajax({
			url: "crawl_data.php",
			data:$( "#crawlForm" ).serialize(),
			method: 'post',
			type: 'json',
			success: function (data) {
				$("#pointsData").val(data);
				parseJsonData($('#color').val(), $('#mode').val());
                    }
                });
}
            

	</script>
</head>
<body >
<form action='crawl_data.php' method=post id='crawlForm'>
<select name="deptAirport" id="deptAirport" class="selectWidth240" onchange="document.getElementById('deptAirportLoc').value=this.options[this.selectedIndex].text;">
		<option value="">Departure airport</option>
		<option value="DXB">Dubai (DXB)</option>
		<option value="LGW">London Gatwick (LGW)</option>
		<option value="LHR">London Heathrow (LHR)</option>
		<option value="JFK">New York (JFK)</option>
		<option value="SYD">Sydney (SYD)</option>
		<option value="---------------">---------------</option>
		<option value="ABJ">Abidjan (ABJ)</option>
		<option value="AUH">Abu Dhabi (AUH)</option>
		<option value="ZVJ">Abu Dhabi (BUS) (ZVJ)</option>
		<option value="ABV">Abuja Airport (ABV)</option>
		<option value="ACC">Accra (ACC)</option>
		<option value="ADD">Addis Ababa (ADD)</option>
		<option value="ADL">Adelaide (ADL)</option>
		<option value="AMD">Ahmedabad (AMD)</option>
		<option value="AAN">Al Ain (AAN)</option>
		<option value="ZVH">Al Ain (BUS) (ZVH)</option>
		<option value="ALG">Algiers (ALG)</option>
		<option value="AMM">Amman (AMM)</option>
		<option value="AMS">Amsterdam Schiphol (AMS)</option>
		<option value="ATH">Athens (ATH)</option>
		<option value="AKL">Auckland (AKL)</option>
		<option value="BGW">Baghdad (BGW)</option>
		<option value="BAH">Bahrain (BAH)</option>
		<option  value="BKK">Bangkok (BKK)</option>
		<option value="BCN">Barcelona (BCN)</option>
		<option value="BSR">Basra (BSR)</option>
		<option value="PEK">Beijing (PEK)</option>
		<option value="BEY">Beirut (BEY)</option>
		<option value="BLR">Bengaluru (Bangalore) (BLR)</option>
		<option value="BHX">Birmingham (BHX)</option>
		<option value="BOS">Boston (BOS)</option>
		<option value="BNE">Brisbane (BNE)</option>
		<option value="BRU">Brussels (BRU)</option>
		<option value="EZE">Buenos Aires (EZE)</option>
		<option value="CAI">Cairo (CAI)</option>
		<option value="CPT">Cape Town (CPT)</option>
		<option value="CMN">Casablanca (CMN)</option>
		<option value="MAA">Chennai (MAA)</option>
		<option value="ORD">Chicago (ORD)</option>
		<option value="CHC">Christchurch (CHC)</option>
		<option value="CRK">Clark International (CRK)</option>
		<option value="CMB">Colombo (CMB)</option>
		<option value="CKY">Conakry (CKY)</option>
		<option value="CPH">Copenhagen (CPH)</option>
		<option value="DKR">Dakar (DKR)</option>
		<option value="DFW">Dallas (DFW)</option>
		<option value="DAM">Damascus (DAM)</option>
		<option value="DMM">Dammam (DMM)</option>
		<option value="DAR">Dar Es Salaam (DAR)</option>
		<option value="DEL">Delhi (DEL)</option>
		<option value="DAC">Dhaka (DAC)</option>
		<option value="DOH">Doha (DOH)</option>
		<option value="DUB">Dublin (DUB)</option>
		<option value="DUR">Durban (DUR)</option>
		<option value="DUS">Düsseldorf (DUS)</option>
		<option value="EBB">Entebbe (EBB)</option>
		<option value="EBL">Erbil (EBL)</option>
		<option value="FRA">Frankfurt (FRA)</option>
		<option value="GVA">Geneva (GVA)</option>
		<option value="GLA">Glasgow (GLA)</option>
		<option value="CAN">Guangzhou (CAN)</option>
		<option value="HAM">Hamburg (HAM)</option>
		<option value="HRE">Harare (HRE)</option>
		<option value="SGN">Ho Chi Minh City (SGN)</option>
		<option value="HKG">Hong Kong (HKG)</option>
		<option value="IAH">Houston (IAH)</option>
		<option value="HYD">Hyderabad (HYD)</option>
		<option value="ISB">Islamabad (ISB)</option>
		<option value="IST">Istanbul (IST)</option>
		<option value="CGK">Jakarta (CGK)</option>
		<option value="JED">Jeddah (JED)</option>
		<option value="JNB">Johannesburg (JNB)</option>
		<option value="KBL">Kabul (KBL)</option>
		<option value="KAN">Kano Airport (KAN)</option>
		<option value="KHI">Karachi (KHI)</option>
		<option value="KRT">Khartoum (KRT)</option>
		<option value="DMS">Khobar Sabtco Bus Station (DMS)</option>
		<option value="KBP">Kiev (KBP)</option>
		<option value="COK">Kochi (Cochin) (COK)</option>
		<option value="CCU">Kolkata (Calcutta) (CCU)</option>
		<option value="CCJ">Kozhikode (Calicut) (CCJ)</option>
		<option value="KUL">Kuala Lumpur (KUL)</option>
		<option value="KWI">Kuwait (KWI)</option>
		<option value="LOS">Lagos (LOS)</option>
		<option value="LHE">Lahore (LHE)</option>
		<option value="LCA">Larnaca (LCA)</option>
		<option value="LIS">Lisbon (LIS)</option>
		<option value="LAX">Los Angeles (LAX)</option>
		<option value="LAD">Luanda (LAD)</option>
		<option value="LUN">Lusaka (LUN)</option>
		<option value="LYS">Lyon (LYS)</option>
		<option value="MAD">Madrid (MAD)</option>
		<option value="MLE">Male (MLE)</option>
		<option value="MLA">Malta (MLA)</option>
		<option value="MAN">Manchester (MAN)</option>
		<option value="MNL">Manila (MNL)</option>
		<option value="MRU">Mauritius (MRU)</option>
		<option value="MED">Medina (Madinah) (MED)</option>
		<option value="MEL">Melbourne (MEL)</option>
		<option value="MXP">Milan (MXP)</option>
		<option value="DME">Moscow (DME)</option>
		<option value="BOM">Mumbai (BOM)</option>
		<option value="MUC">Munich (MUC)</option>
		<option value="MCT">Muscat (MCT)</option>
		<option value="NGY">Nagoya (BUS) (NGY)</option>
		<option value="NGO">Nagoya (NGO)</option>
		<option value="NBO">Nairobi (NBO)</option>
		<option value="NCL">Newcastle (NCL)</option>
		<option value="NCE">Nice (NCE)</option>
		<option value="KIX">Osaka (KIX)</option>
		<option value="OSL">Oslo (OSL)</option>
		<option value="CDG">Paris (CDG)</option>
		<option value="PER">Perth (PER)</option>
		<option value="PEW">Peshawar (PEW)</option>
		<option value="HKT">Phuket (HKT)</option>
		<option value="PRG">Prague (PRG)</option>
		<option value="GIG">Rio de Janeiro (GIG)</option>
		<option value="RUH">Riyadh (RUH)</option>
		<option value="FCO">Rome (FCO)</option>
		<option value="SFO">San Francisco (SFO)</option>
		<option value="SAH">Sana'a (SAH)</option>
		<option value="GRU">São Paulo (GRU)</option>
		<option value="SEA">Seattle (SEA)</option>
		<option value="ICN">Seoul (ICN)</option>
		<option value="SEZ">Seychelles (SEZ)</option>
		<option value="PVG">Shanghai (PVG)</option>
		<option value="SKT">Sialkot (SKT)</option>
		<option value="SIN">Singapore (SIN)</option>
		<option value="LED">St. Petersburg (LED)</option>
		<option value="ARN">Stockholm (ARN)</option>
		<option value="TPE">Taipei (TPE)</option>
		<option value="IKA">Tehran (IKA)</option>
		<option value="TRV">Thiruvananthapuram (Trivandrum) (TRV)</option>
		<option value="HND">Tokyo Haneda (HND)</option>
		<option value="NRT">Tokyo Narita (NRT)</option>
		<option value="YYZ">Toronto (YYZ)</option>
		<option value="TIP">Tripoli (TIP)</option>
		<option value="TUN">Tunis (TUN)</option>
		<option value="VCE">Venice (VCE)</option>
		<option value="VIE">Vienna (VIE)</option>
		<option value="WAW">Warsaw (WAW)</option>
		<option value="IAD">Washington (IAD)</option>
		<option value="WLG">Wellington (WLG)</option>
		<option value="ZRH">Zürich (ZRH)</option>

	</select>

<select name="arrivalAirport" id="arrivalAirport" onchange="document.getElementById('arrivalAirportLoc').value=this.options[this.selectedIndex].text;">
		<option value="">Arrival airport</option>
		<option value="DXB">Dubai (DXB)</option>
		<option value="LGW">London Gatwick (LGW)</option>
		<option value="LHR">London Heathrow (LHR)</option>
		<option value="JFK">New York (JFK)</option>
		<option value="SYD">Sydney (SYD)</option>
		<option value="---------------">---------------</option>
		<option value="ABJ">Abidjan (ABJ)</option>
		<option value="AUH">Abu Dhabi (AUH)</option>
		<option value="ZVJ">Abu Dhabi (BUS) (ZVJ)</option>
		<option value="ABV">Abuja Airport (ABV)</option>
		<option value="ACC">Accra (ACC)</option>
		<option value="ADD">Addis Ababa (ADD)</option>
		<option value="ADL">Adelaide (ADL)</option>
		<option value="AMD">Ahmedabad (AMD)</option>
		<option value="AAN">Al Ain (AAN)</option>
		<option value="ZVH">Al Ain (BUS) (ZVH)</option>
		<option value="ALG">Algiers (ALG)</option>
		<option value="AMM">Amman (AMM)</option>
		<option value="AMS">Amsterdam Schiphol (AMS)</option>
		<option value="ATH">Athens (ATH)</option>
		<option value="AKL">Auckland (AKL)</option>
		<option value="BGW">Baghdad (BGW)</option>
		<option value="BAH">Bahrain (BAH)</option>
		<option value="BKK">Bangkok (BKK)</option>
		<option value="BCN">Barcelona (BCN)</option>
		<option value="BSR">Basra (BSR)</option>
		<option value="PEK">Beijing (PEK)</option>
		<option value="BEY">Beirut (BEY)</option>
		<option value="BLR">Bengaluru (Bangalore) (BLR)</option>
		<option value="BHX">Birmingham (BHX)</option>
		<option value="BOS">Boston (BOS)</option>
		<option value="BNE">Brisbane (BNE)</option>
		<option value="BRU">Brussels (BRU)</option>
		<option value="EZE">Buenos Aires (EZE)</option>
		<option value="CAI">Cairo (CAI)</option>
		<option value="CPT">Cape Town (CPT)</option>
		<option value="CMN">Casablanca (CMN)</option>
		<option value="MAA">Chennai (MAA)</option>
		<option value="ORD">Chicago (ORD)</option>
		<option value="CHC">Christchurch (CHC)</option>
		<option value="CRK">Clark International (CRK)</option>
		<option value="CMB">Colombo (CMB)</option>
		<option value="CKY">Conakry (CKY)</option>
		<option value="CPH">Copenhagen (CPH)</option>
		<option value="DKR">Dakar (DKR)</option>
		<option value="DFW">Dallas (DFW)</option>
		<option value="DAM">Damascus (DAM)</option>
		<option value="DMM">Dammam (DMM)</option>
		<option value="DAR">Dar Es Salaam (DAR)</option>
		<option value="DEL">Delhi (DEL)</option>
		<option value="DAC">Dhaka (DAC)</option>
		<option value="DOH">Doha (DOH)</option>
		<option value="DUB">Dublin (DUB)</option>
		<option value="DUR">Durban (DUR)</option>
		<option value="DUS">Düsseldorf (DUS)</option>
		<option value="EBB">Entebbe (EBB)</option>
		<option value="EBL">Erbil (EBL)</option>
		<option value="FRA">Frankfurt (FRA)</option>
		<option value="GVA">Geneva (GVA)</option>
		<option value="GLA">Glasgow (GLA)</option>
		<option value="CAN">Guangzhou (CAN)</option>
		<option value="HAM">Hamburg (HAM)</option>
		<option value="HRE">Harare (HRE)</option>
		<option value="SGN">Ho Chi Minh City (SGN)</option>
		<option value="HKG">Hong Kong (HKG)</option>
		<option value="IAH">Houston (IAH)</option>
		<option value="HYD">Hyderabad (HYD)</option>
		<option value="ISB">Islamabad (ISB)</option>
		<option value="IST">Istanbul (IST)</option>
		<option value="CGK">Jakarta (CGK)</option>
		<option value="JED">Jeddah (JED)</option>
		<option value="JNB">Johannesburg (JNB)</option>
		<option value="KBL">Kabul (KBL)</option>
		<option value="KAN">Kano Airport (KAN)</option>
		<option value="KHI">Karachi (KHI)</option>
		<option value="KRT">Khartoum (KRT)</option>
		<option value="DMS">Khobar Sabtco Bus Station (DMS)</option>
		<option value="KBP">Kiev (KBP)</option>
		<option value="COK">Kochi (Cochin) (COK)</option>
		<option value="CCU">Kolkata (Calcutta) (CCU)</option>
		<option value="CCJ">Kozhikode (Calicut) (CCJ)</option>
		<option value="KUL">Kuala Lumpur (KUL)</option>
		<option value="KWI">Kuwait (KWI)</option>
		<option value="LOS">Lagos (LOS)</option>
		<option value="LHE">Lahore (LHE)</option>
		<option value="LCA">Larnaca (LCA)</option>
		<option value="LIS">Lisbon (LIS)</option>
		<option value="LAX">Los Angeles (LAX)</option>
		<option value="LAD">Luanda (LAD)</option>
		<option value="LUN">Lusaka (LUN)</option>
		<option value="LYS">Lyon (LYS)</option>
		<option value="MAD">Madrid (MAD)</option>
		<option value="MLE">Male (MLE)</option>
		<option value="MLA">Malta (MLA)</option>
		<option value="MAN">Manchester (MAN)</option>
		<option value="MNL">Manila (MNL)</option>
		<option value="MRU">Mauritius (MRU)</option>
		<option value="MED">Medina (Madinah) (MED)</option>
		<option value="MEL">Melbourne (MEL)</option>
		<option value="MXP">Milan (MXP)</option>
		<option value="DME">Moscow (DME)</option>
		<option value="BOM">Mumbai (BOM)</option>
		<option value="MUC">Munich (MUC)</option>
		<option value="MCT">Muscat (MCT)</option>
		<option value="NGY">Nagoya (BUS) (NGY)</option>
		<option value="NGO">Nagoya (NGO)</option>
		<option value="NBO">Nairobi (NBO)</option>
		<option value="NCL">Newcastle (NCL)</option>
		<option value="NCE">Nice (NCE)</option>
		<option value="KIX">Osaka (KIX)</option>
		<option value="OSL">Oslo (OSL)</option>
		<option value="CDG">Paris (CDG)</option>
		<option value="PER">Perth (PER)</option>
		<option value="PEW">Peshawar (PEW)</option>
		<option value="HKT">Phuket (HKT)</option>
		<option value="PRG">Prague (PRG)</option>
		<option value="GIG">Rio de Janeiro (GIG)</option>
		<option value="RUH">Riyadh (RUH)</option>
		<option value="FCO">Rome (FCO)</option>
		<option value="SFO">San Francisco (SFO)</option>
		<option value="SAH">Sana'a (SAH)</option>
		<option value="GRU">São Paulo (GRU)</option>
		<option value="SEA">Seattle (SEA)</option>
		<option value="ICN">Seoul (ICN)</option>
		<option value="SEZ">Seychelles (SEZ)</option>
		<option value="PVG">Shanghai (PVG)</option>
		<option value="SKT">Sialkot (SKT)</option>
		<option value="SIN">Singapore (SIN)</option>
		<option value="LED">St. Petersburg (LED)</option>
		<option value="ARN">Stockholm (ARN)</option>
		<option value="TPE">Taipei (TPE)</option>
		<option value="IKA">Tehran (IKA)</option>
		<option value="TRV">Thiruvananthapuram (Trivandrum) (TRV)</option>
		<option value="HND">Tokyo Haneda (HND)</option>
		<option value="NRT">Tokyo Narita (NRT)</option>
		<option value="YYZ">Toronto (YYZ)</option>
		<option value="TIP">Tripoli (TIP)</option>
		<option value="TUN">Tunis (TUN)</option>
		<option value="VCE">Venice (VCE)</option>
		<option value="VIE">Vienna (VIE)</option>
		<option value="WAW">Warsaw (WAW)</option>
		<option value="IAD">Washington (IAD)</option>
		<option value="WLG">Wellington (WLG)</option>
		<option value="ZRH">Zürich (ZRH)</option>

	</select>
	<select name="class" id="class">
		<option value="Economy">Economy</option>
		<option value="Business">Business</option>
		<option value="First">First</option>
	</select>
<input type=hidden value='' id='deptAirportLoc' name='deptAirportLoc' autocomplete=Off />
<input type=hidden value='' id='arrivalAirportLoc' name='ArrivalAirportLoc' autocomplete=Off />
<input type=button value=search name=search onclick='getParsedData();' />

<br>

</form>
<input type=hidden value='' id='pointsData' name='pointsData' autocomplete=Off />
	<select name="color" id="color" onchange="parseJsonData($('#color').val(), $('#mode').val());">
		<option value="Blue">Blue</option>
		<option value="Silver">Silver</option>
		<option value="Gold">Gold</option>
		<option value="Platinum">Platinum</option>
	</select>
	<select name="mode" id="mode" onchange="parseJsonData($('#color').val(), $('#mode').val());">
		<option value="Return">Return</option>
		<option value="Oneway">Oneway</option>
	</select>
<div id='pointsTable' style='width:70%; margin:auto;'></div>
</body>
</html>
